package com.example.demo.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;

@NoArgsConstructor
@Data
@AllArgsConstructor
@Table
@Entity
public class Historique implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long objectId;
    private String objectName;
    private String who;
    private String what;
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Calendar date;

    @PrePersist
    protected void onCreate() {
        System.out.println("PrePersist PrePersist PrePersist PrePersist");
    }

    @PreUpdate
    protected void onUpdate() {
        System.out.println("PreUpdate PreUpdate PreUpdate PreUpdate");
    }

}
