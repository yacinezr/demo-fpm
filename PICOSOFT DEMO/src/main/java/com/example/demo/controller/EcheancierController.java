package com.example.demo.controller;

import com.example.demo.domain.Credit;
import com.example.demo.domain.Echeancier;
import com.example.demo.service.EcheancierService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api")
public class EcheancierController {
    private static final String ENTITY_NAME = "Echeancier";

    @Autowired
    private EcheancierService echeancierService;

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping("/echeanciersByIdCredit/{idCredit}")
    public ResponseEntity createEcheancier(@PathVariable Long idCredit) throws URISyntaxException {
        log.debug("REST request to save Echeancier : {}");
        echeancierService.create(idCredit);
        return ResponseEntity.created(new URI("/api/echeanciersByIdCredit/" + idCredit))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, String.valueOf(idCredit)))
                .body(null);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PutMapping("/echeanciers//{idEcheancier}")
    public ResponseEntity<Echeancier> updateEcheancier(@PathVariable Long idEcheancier) throws URISyntaxException {
        log.debug("REST request to update Echeancier : {}");
        Echeancier result = echeancierService.update(idEcheancier);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, null))
                .body(result);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/echeanciersByIdCredit/{idCredit}")
    public List<Echeancier> getAllEcheancierByIdCredit(@PathVariable Long idCredit) {
        log.debug("REST request to get all Echeanciers By IdCredit");
        return echeancierService.findByCredit(idCredit);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/echeanciers")
    public List<Echeancier> getAllEcheancier() {
        log.debug("REST request to get all Assures");
        return echeancierService.findAll();
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/echeanciers/{id}")
    public ResponseEntity<Echeancier> getEcheancier(@PathVariable Long id) {
        log.debug("REST request to get Echeancier : {}", id);
        Echeancier echeancier = echeancierService.findOne(id);
        return Optional.ofNullable(echeancier).map(response -> ResponseEntity.ok().headers(null).body(response))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @DeleteMapping("/echeanciers/{id}")
    public ResponseEntity<Void> deleteEcheancier(@PathVariable Long id) {
        log.debug("REST request to delete Echeancier : {}", id);
        echeancierService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
