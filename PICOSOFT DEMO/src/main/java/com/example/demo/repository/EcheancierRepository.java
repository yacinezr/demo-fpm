package com.example.demo.repository;

import com.example.demo.domain.Credit;
import com.example.demo.domain.Echeancier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EcheancierRepository extends JpaRepository<Echeancier, Long> {
    List<Echeancier> findByCredit(Credit credit);
}