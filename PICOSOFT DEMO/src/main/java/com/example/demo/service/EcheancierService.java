package com.example.demo.service;


import com.example.demo.domain.Credit;
import com.example.demo.domain.Echeancier;
import com.example.demo.domain.Historique;
import com.example.demo.repository.CreditRepository;
import com.example.demo.repository.EcheancierRepository;
import com.example.demo.repository.HistoriqueRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

@Slf4j
@Service
@Transactional
public class EcheancierService {

    @Autowired
    private EcheancierRepository echeancierRepository;
    @Autowired
    private HistoriqueRepository historiqueRepository;
    @Autowired
    private CreditRepository creditRepository;
    @Autowired
    private EntityManager em;

    public void create(Long idCredit) {
        Credit credit = creditRepository.getOne(idCredit);

//****************************** TABLE D'AMMORTISSEMENT *********************************
        String principals = credit.getTotal();
        int length = credit.getMoisDus();
        String interets = "9.569";

        double capitalDu = Double.parseDouble(principals);
        double amountInterest = Double.parseDouble(interets);

        double monthlyInterest = amountInterest / (12 * 100);
        double mensualite = capitalDu * (monthlyInterest / (1 - Math.pow((1 + monthlyInterest), (length * -1))));

        System.out.println("mensualite   " + "montantRemboursement   " + "interet   " + "capitalDu   ");

        NumberFormat nf = NumberFormat.getIntegerInstance();
//****************************************************************************************

        for (int i = 1; i <= credit.getMoisDus(); i++) {
            Echeancier newEcheancier = new Echeancier();
//****************************** TABLE D'AMMORTISSEMENT *********************************
            double interet = capitalDu * monthlyInterest;
            double montantRemboursement = mensualite - interet;
            if (i != 1) {
                capitalDu = capitalDu - montantRemboursement;
            }
            System.out.print(nf.format(mensualite) + "      " + nf.format(montantRemboursement) + "                 " + nf.format(interet) + "    " + nf.format(capitalDu));
            System.out.println();

            newEcheancier.setMensualite(nf.format(mensualite));
            newEcheancier.setMontantRemboursement(nf.format(montantRemboursement));
            newEcheancier.setInteret(nf.format(interet));
            newEcheancier.setCapitalDu(nf.format(capitalDu));
//****************************************************************************************

            newEcheancier.setCredit(credit);
            newEcheancier.setNum((long) i);
            newEcheancier.setEtat("Attent paiement");
            newEcheancier.setFrais("0");

            //************* set Date Echeance **************
            Calendar cal = new GregorianCalendar(credit.getDateDebutRembourcement().get(Calendar.YEAR),
                    credit.getDateDebutRembourcement().get(Calendar.MONTH), 28);
            cal.add(Calendar.MONTH, i);
            newEcheancier.setDateEcheance(cal);
            // *********************************************

            if (i == 1) {
                newEcheancier.setCapitalDu(credit.getTotal());
            }
            double tauxInteret = 0.01;


            em.persist(newEcheancier);
            em.flush();
            log.info("*************** Request to create echeancier :" + i);
        }
    }

    public Echeancier update(Long idEcheancier) {
        log.debug("Request to update echeancier : {}");
        Echeancier echeancier = echeancierRepository.findOne(idEcheancier);
        echeancier.setEtat("Rembourcement effectué");
        Echeancier resultEcheancier = echeancierRepository.save(echeancier);
        historiqueRepository.save(new Historique(null, resultEcheancier.getId(), "Echeancier", "admin", "Modification", null));
        return resultEcheancier;
    }

    @Transactional(readOnly = true)
    public List<Echeancier> findAll() {
        log.debug("Request to get all echeanciers");
        return echeancierRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Echeancier findOne(Long id) {
        return echeancierRepository.findOne(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete echeancier : {}", id);
        historiqueRepository.save(new Historique(null, id, "Echeancier", "admin", "Suppression", null));
        echeancierRepository.delete(id);
    }

    @Transactional(readOnly = true)
    public List<Echeancier> findByCredit(Long idCredit) {
        return echeancierRepository.findByCredit(creditRepository.findOne(idCredit));
    }
}

