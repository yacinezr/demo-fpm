package com.example.demo.service;

import com.example.demo.domain.Assure;
import com.example.demo.domain.Credit;
import com.example.demo.domain.Demande;
import com.example.demo.domain.Historique;
import com.example.demo.repository.AssureRepository;
import com.example.demo.repository.CreditRepository;
import com.example.demo.repository.DemandeRepository;
import com.example.demo.repository.HistoriqueRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.NumberFormat;
import java.util.List;

@Slf4j
@Service
@Transactional
public class CreditService {

    @Autowired
    private CreditRepository creditRepository;
    @Autowired
    private HistoriqueRepository historiqueRepository;
    @Autowired
    private AssureRepository assureRepository;
    @Autowired
    private DemandeRepository demandeRepository;
    @Autowired
    private EcheancierService echeancierService;


    public Credit create(Credit credit) {
        log.debug("Request to create credit : {}", credit);
        credit.setAssure(assureRepository.findOne(demandeRepository.findOne(credit.getDemande().getId()).getAssure().getId()));
        credit.setEtat("Remboursement en cours");
////////////////////////////////////////////////////////////////
        String principals = credit.getTotal();
        int length = credit.getMoisDus();
        String interets = "9.569";

        double capitalDu = Double.parseDouble(principals);
        double amountInterest = Double.parseDouble(interets);

        double monthlyInterest = amountInterest / (12 * 100);
        double mensualite = capitalDu * (monthlyInterest / (1 - Math.pow((1 + monthlyInterest), (length * -1))));

        System.out.println("mensualite   " + "montantRemboursement   " + "interet   " + "capitalDu   ");

        NumberFormat nf = NumberFormat.getIntegerInstance();
        double interet = capitalDu * monthlyInterest;
        double montantRemboursement = mensualite - interet;
        credit.setMensualite(nf.format(mensualite));
////////////////////////////////////////////////////////////////





        Credit resultCredit = creditRepository.save(credit);

        echeancierService.create(resultCredit.getId());
        historiqueRepository.save(new Historique(null, resultCredit.getId(), "Credit", "admin", "Creation", null));
        return resultCredit;
    }

    public Credit update(Credit credit) {
        log.debug("Request to update credit : {}", credit);
        credit.setAssure(assureRepository.findOne(demandeRepository.findOne(credit.getDemande().getId()).getAssure().getId()));
        Credit resultCredit = creditRepository.save(credit);
        historiqueRepository.save(new Historique(null, resultCredit.getId(), "Credit", "admin", "Modification", null));
        return resultCredit;
    }

    @Transactional(readOnly = true)
    public List<Credit> findAll() {
        log.debug("Request to get all credits");
        return creditRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Credit findOne(Long id) {
        return creditRepository.findOne(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete credit : {}", id);
        historiqueRepository.save(new Historique(null, id, "Credit", "admin", "Suppression", null));
        creditRepository.delete(id);
    }

    @Transactional(readOnly = true)
    public List<Credit> findByAssure(Long id) {
        Assure assure = assureRepository.findOne(id);
        return creditRepository.findByAssure(assure);
    }

    @Transactional(readOnly = true)
    public List<Credit> findByAssure(String matricule) {
        Assure assure = assureRepository.findByMatricule(matricule);
        return creditRepository.findByAssure(assure);
    }

    public Credit findByDemande(Long idDemande) {
        return creditRepository.findByDemande(demandeRepository.findOne(idDemande));
    }
}
