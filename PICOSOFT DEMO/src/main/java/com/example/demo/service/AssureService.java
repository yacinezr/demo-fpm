package com.example.demo.service;

import com.example.demo.domain.Assure;
import com.example.demo.domain.Historique;
import com.example.demo.repository.AssureRepository;
import com.example.demo.repository.HistoriqueRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
@Transactional
public class AssureService {

    @Autowired
    private AssureRepository assureRepository;
    @Autowired
    private HistoriqueRepository historiqueRepository;

    public Assure create(Assure assure) {
        log.debug("Request to create assure : {}", assure);
        Assure resultAssure = assureRepository.save(assure);
        historiqueRepository.save(new Historique(null, resultAssure.getId(), "Assure", "admin", "Creation", null));
        return resultAssure;
    }

    public Assure update(Assure assure) {
        log.debug("Request to update assure : {}", assure);
        Assure resultAssure = assureRepository.save(assure);
        historiqueRepository.save(new Historique(null, resultAssure.getId(), "Assure", "admin", "Modification", null));
        return resultAssure;
    }

    @Transactional(readOnly = true)
    public List<Assure> findAll() {
        log.debug("Request to get all assures");
        return assureRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Assure findOne(Long id) {
        return assureRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    public Assure findByMatricule(String matricule) {
        return assureRepository.findByMatricule(matricule);
    }

    public void delete(Long id) {
        log.debug("Request to delete assure : {}", id);
        historiqueRepository.save(new Historique(null, id, "Assure", "admin", "Suppression", null));
        assureRepository.delete(id);
    }
}
