import {Directive, ElementRef, EventEmitter, HostListener, OnInit, Output} from '@angular/core';
import 'widgster';
import { debounceTime } from 'rxjs/operators';
import {Subject} from 'rxjs/Subject';

@Directive({
  selector: '[appDebounceClick]'
})
export class DebounceDirective implements OnInit {
  @Output() debounceClick = new EventEmitter();
  private clicks = new Subject();

  constructor() {
  }

  @HostListener('click', ['$event'])
  clickEvent(event) {
    event.preventDefault();
    event.stopPropagation();
    console.log('Click from Host Element!');
    console.log(event)
  }

  ngOnInit(): void {
    this.clicks.pipe(
      debounceTime(500)
    ).subscribe(e => this.debounceClick.emit(e));
  }

}
