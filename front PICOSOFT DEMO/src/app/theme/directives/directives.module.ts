import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetDirective } from './widget/widget.directive';
import {DebounceDirective} from './debounce/debounce.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    WidgetDirective,
    DebounceDirective
  ],
  exports: [
    WidgetDirective,
    DebounceDirective
  ]
})
export class DirectivesModule { }
