import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CustomFormsModule } from 'ng2-validation';
import { CKEditorModule } from 'ng2-ckeditor';
import { DirectivesModule } from '../../theme/directives/directives.module';
import { ControlsComponent } from './controls/controls.component';
import { FileUploaderComponent } from './controls/file-uploader/file-uploader.component';
import { ImageUploaderComponent } from './controls/image-uploader/image-uploader.component';
import { MultipleImageUploaderComponent } from './controls/multiple-image-uploader/multiple-image-uploader.component';
import { LayoutsComponent } from './layouts/layouts.component';
import { ValidationsComponent } from './validations/validations.component';
import { WizardComponent } from './wizard/wizard.component';
import { EditorComponent } from './editor/editor.component';
import { DxPopupModule, DxTemplateModule, DxButtonModule, DxDataGridModule } from 'devextreme-angular';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { LivraisonComponent } from './livraison/livraison.component';
import { ControlstService } from 'app/pages/form-elements/controls/controls.service';
import { EditorService } from 'app/pages/form-elements/editor/editor.service';
import { ValidationsService } from 'app/pages/form-elements/validations/validations.service';
import { VerifierAssureeComponent } from './validations/verifier-assuree/verifier-assuree.component';
import { LayoutService } from 'app/pages/form-elements/layouts/layouts.service';
import { VerifierAssureeService } from 'app/pages/form-elements/validations/verifier-assuree/verifier-assuree.service';
import { ValiderCarteComponent } from './wizard/valider-carte/valider-carte.component';
import { ValiderCarteService } from 'app/pages/form-elements/wizard/valider-carte/valider-carte.service';
import { LivraisonService } from 'app/pages/form-elements/livraison/livraison.service';
import { ValiderLivraisonComponent } from './livraison/valider-livraison/valider-livraison.component';
import { ValiderLivraisonService } from 'app/pages/form-elements/livraison/valider-livraison/valider-livraison.service';
import { ShowAssuComponent } from './wizard/show-assu/show-assu.component';
import { ShowAssurComponent } from './livraison/show-assur/show-assur.component';
import { ConsultationComponent } from './consultation/consultation.component';
import { ImprimerComponent } from './consultation/imprimer/imprimer.component';
import { AllComponent } from 'app/pages/form-elements/all/all.component';
import { ShowEnfantComponent } from 'app/pages/form-elements/validations/show-enfant/show-enfant.component';
import { ShowEnfantService } from 'app/pages/form-elements/validations/show-enfant/show-enfant.service';
import { Imprimer2Component } from 'app/pages/form-elements/consultation/imprimer2/imprimer2.component';
import { ShowConjointComponent } from 'app/pages/form-elements/validations/show-conjoint/show-conjoint.component';
import { ImprimerService } from 'app/pages/form-elements/consultation/imprimer/imprimer.service';
import {HttpClient, HttpClientModule, HttpHandler} from '@angular/common/http';
import {ShowAssureeComponent} from './validations/show-assuree/show-assuree.component';
import {EcheancierComponent} from './all/echeancier/echeancier.component';
import {NgSelectModule} from '@ng-select/ng-select';
import {AllService} from './all.service';
import { GestionComponent } from 'app/pages/form-elements/gestion/gestion.component';
import { Gestion2Component } from 'app/pages/form-elements/gestion/gestion2/gestion2.component';
import {SubjService} from './subj.service';


export const routes = [
  { path: '', redirectTo: 'controls', pathMatch: 'full'},
  { path: 'controls', component: ControlsComponent, data: { breadcrumb: '' } },
  { path: 'layouts', component: LayoutsComponent, data: { breadcrumb: '' } },
  { path: 'validations', component: ValidationsComponent, data: { breadcrumb: 'Validation' } },
  { path: 'wizard', component: WizardComponent, data: { breadcrumb: '' } },
  { path: 'editor', component: EditorComponent, data: { breadcrumb: '' } },
  { path: 'livraison', component: LivraisonComponent, data: { breadcrumb: '' } },
  { path: 'consultation', component: ConsultationComponent, data: { breadcrumb: 'Consultation' } },
  { path: 'all', component: AllComponent, data: { breadcrumb: 'Suivi' } },
    { path: 'gestion', component: GestionComponent, data: { breadcrumb: 'Echéanciers' } }

];


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MultiselectDropdownModule,
    NgbModule,
    CustomFormsModule,
    CKEditorModule,
    DirectivesModule,
    RouterModule.forChild(routes),
    DxDataGridModule,
    HttpModule,
    DxPopupModule,
    DxButtonModule,
    DxTemplateModule,
    HttpClientModule,
    NgSelectModule,
  ],
  declarations: [
    ControlsComponent,
    FileUploaderComponent,
    ImageUploaderComponent,
    MultipleImageUploaderComponent,
    LayoutsComponent,
    WizardComponent,
    EditorComponent,
    LivraisonComponent,
    VerifierAssureeComponent,
    ValiderCarteComponent,
    ValiderLivraisonComponent,
    ShowAssuComponent,
    ShowAssurComponent,
    ConsultationComponent,
    ImprimerComponent,
    AllComponent,
    ShowEnfantComponent,
    Imprimer2Component,
    ShowConjointComponent,
    ShowAssureeComponent,

    ValidationsComponent,
    EcheancierComponent,
	GestionComponent,
    Gestion2Component
  ],
  providers :[SubjService,AllService,HttpClient, ControlstService, EditorService, ValidationsService, LayoutService, VerifierAssureeService,ValiderCarteService,LivraisonService, ValiderLivraisonService, ShowEnfantService],
  entryComponents: [
    EcheancierComponent,
    ShowAssureeComponent,
    ShowConjointComponent,
    Imprimer2Component,
    VerifierAssureeComponent,
    ValiderCarteComponent,
    ValiderLivraisonComponent,
    ShowAssuComponent,
    ShowAssurComponent,
    ImprimerComponent,
    AllComponent,
    ShowEnfantComponent,Gestion2Component,GestionComponent
  ]

})
export class FormElementsModule { }
