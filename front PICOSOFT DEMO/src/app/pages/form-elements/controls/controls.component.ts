import {ToastrService} from 'ngx-toastr';
import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, Validators } from '@angular/forms';
import { ControlstService } from 'app/pages/form-elements/controls/controls.service';

@Component({
  selector: 'app-controls',
  templateUrl: './controls.component.html',
  styleUrls: ['./controls.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ControlsComponent {

  tel: string;
  nom: string;
  prenom: string;
  naissance: string;
  sexe: string;
  address: string;
  cni: string;
  etat: string;
  fonction: string;
  anl: boolean=false;
  pi: boolean=false;
  ctp: boolean=false;
  cam: boolean=false;
  cdbs: boolean=false;
  ae: boolean=false;  

  constructor(private controlsService: ControlstService, public toastrService: ToastrService) { }

  ngOnInit() { }

  addAssuree() {

    let assuree = null;
    assuree = {
      "tel": this.tel,
      "nom": this.nom,
      "prenom": this.prenom,
      "naissance": this.naissance,
      "sexe": this.sexe,
      "address": this.address,
      "cni": this.cni,
      "etat": this.etat,
      "fonction": this.fonction,
      "anl": this.anl,
      "pi": this.pi,
      "ctp": this.ctp,
      "cam": this.cam,
      "cdbs": this.cdbs,
      "ae": this.ae,
      "valideAssuree":false,
      "livreAssuree":false,
      "prodAssuree":false
    }

    console.log(assuree);

    return this.controlsService.addAssuree(assuree).subscribe(contact => {
      
    },
  error =>{
    setTimeout(() => {
      this.toastrService.success('Assuré ajouté avec succés!');

    })
  }
);
  }

}
