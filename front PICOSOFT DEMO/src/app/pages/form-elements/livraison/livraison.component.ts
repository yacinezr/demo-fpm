import {OnDestroy} from '@angular/core/src/metadata/lifecycle_hooks';
import {ShowAssureeComponent} from '../validations/show-assuree/show-assuree.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ValidationsService} from '../validations/validations.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { LivraisonService } from 'app/pages/form-elements/livraison/livraison.service';
import { ValiderLivraisonComponent } from 'app/pages/form-elements/livraison/valider-livraison/valider-livraison.component';
import { ShowAssurComponent } from 'app/pages/form-elements/livraison/show-assur/show-assur.component';
import {Subscription} from 'rxjs/Rx';


@Component({
  selector: 'app-livraison',
  templateUrl: './livraison.component.html',
  styleUrls: ['./livraison.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LivraisonComponent implements OnInit,OnDestroy {
  dataSource: any = {};
  eventSubscriber: Subscription;
  
  constructor(private livraisonService: LivraisonService, private validationsService : ValidationsService,  private modalService: NgbModal) { }

  ngOnInit() {
    let d= new Array();
    
    this.validationsService.getvalidation().subscribe(data =>{
      console.dir(data);
      data.forEach(element => {
        if(element.valideAssuree === true  && element.prodAssuree === true && element.livreAssuree === false){
          d.push(element);
        }
      });
      console.log(this.dataSource)
    });
    this.validationsService.getEnfants().subscribe(data => {
      data.forEach(element => {
        if(element.valideEnfants === true  && element.prodEnfants === true && element.livreEnfants === false){
          d.push(element);
        }
      });
    })
    this.validationsService.getConjoints().subscribe(data => {
      data.forEach(element => {
        if(element.valideConjoints === true  && element.prodConjoints === true && element.livreConjoints === false){
          d.push(element);
        }
      });
    });
    this.dataSource = d;
    
    this.registerChangeInUsers();
  }

  ngOnDestroy() {
    // this.eventManager.destroy(this.eventSubscriber);
  }

  registerChangeInUsers() {
    //this.eventSubscriber = this.eventManager.subscribe('tachesListModification', (response) => this.refresh());
  }

  refresh() {
    let d= new Array();
    
    this.validationsService.getvalidation().subscribe(data =>{
      console.dir(data);
      data.forEach(element => {
        if(element.valideAssuree === true  && element.prodAssuree === true && element.livreAssuree === false){
          d.push(element);
        }
      });
      console.log(this.dataSource)
    });
    this.validationsService.getEnfants().subscribe(data => {
      data.forEach(element => {
        if(element.valideEnfants === true  && element.prodEnfants === true && element.livreEnfants === false){
          d.push(element);
        }
      });
    })
    this.validationsService.getConjoints().subscribe(data => {
      data.forEach(element => {
        if(element.valideConjoints === true  && element.prodConjoints === true && element.livreConjoints === false){
          d.push(element);
        }
      });
      this.dataSource = d;
    });
      
      console.log(this.dataSource)


  }

  openModalValidationAssuree(data){
    const modalRef = this.modalService.open(ShowAssurComponent, { size: 'lg', backdrop: 'static', keyboard: false });
    modalRef.componentInstance.data = data;
  
  }

  openModalValidationLivraison(data){
    const modalRef = this.modalService.open(ValiderLivraisonComponent, { size: 'lg', backdrop: 'static', keyboard: false });
    modalRef.componentInstance.data = data;
  }
}
