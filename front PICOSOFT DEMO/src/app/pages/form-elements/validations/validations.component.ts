import {Subscription} from 'rxjs/Subscription';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { NgModule, Component, Inject, enableProdMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { Http } from '@angular/http';
import {DxDataGridComponent, DxDataGridModule} from 'devextreme-angular';
import CustomStore from 'devextreme/data/custom_store';
import { ValidationsService } from 'app/pages/form-elements/validations/validations.service';
import { ShowAssureeComponent } from 'app/pages/form-elements/validations/show-assuree/show-assuree.component';
import { VerifierAssureeComponent } from 'app/pages/form-elements/validations/verifier-assuree/verifier-assuree.component';
import { ShowEnfantComponent } from 'app/pages/form-elements/validations/show-enfant/show-enfant.component';
import { ShowConjointComponent } from 'app/pages/form-elements/validations/show-conjoint/show-conjoint.component';
import {SubjService} from '../subj.service';

@Component({
  selector: 'app-validations',
  templateUrl: './validations.component.html',
  encapsulation: ViewEncapsulation.None
})
export class ValidationsComponent implements OnInit {
  dataSource: any = {};
  eventSubscriber: Subscription;
  @ViewChild(DxDataGridComponent) dataGrid: DxDataGridComponent;

  constructor(private modalService: NgbModal,public validationsService:ValidationsService,private subjService :SubjService) { }

  ngOnInit() {
    // let data = [
    //   {id:1,datedemande:'14/05/2018',motifDemande:'Equipment',typePret:'Normal',montantDemande:'2400',dureeRemboursement:'9',typeRachat:'Avec engagement'},
    //   {id:2,datedemande:'17/05/2018',motifDemande:'Immobilier',typePret:'Normal',montantDemande:'5200',dureeRemboursement:'18',typeRachat:'Sans engagement'},
    //   {id:3,datedemande:'22/05/2018',motifDemande:'Equipment',typePret:'Normal',montantDemande:'4204',dureeRemboursement:'24',typeRachat:'Avec engagement'},
    //   {id:4,datedemande:'14/06/2018',motifDemande:'Immobilier',typePret:'Normal',montantDemande:'1000',dureeRemboursement:'6',typeRachat:'Avec engagement'},
    //   {id:5,datedemande:'18/06/2018',motifDemande:'Immobilier',typePret:'Normal',montantDemande:'1000',dureeRemboursement:'6',typeRachat:'Sans engagement'},
    //   {id:6,datedemande:'20/06/2018',motifDemande:'Equipment',typePret:'Normal',montantDemande:'6000',dureeRemboursement:'12',typeRachat:'Avec engagement'},
    //   {id:7,datedemande:'30/05/2018',motifDemande:'Equipment',typePret:'Normal',montantDemande:'24045',dureeRemboursement:'24',typeRachat:'Sans engagement'},
    //   {id:8,datedemande:'01/03/2018',motifDemande:'Immobilier',typePret:'Normal',montantDemande:'2400',dureeRemboursement:'9',typeRachat:'Avec engagement'},
    //   {id:9,datedemande:'11/02/2018',motifDemande:'Equipment',typePret:'Normal',montantDemande:'2400250',dureeRemboursement:'24',typeRachat:'Avec engagement'},
    //   {id:10,datedemande:'03/01/2018',motifDemande:'Immobilier',typePret:'Normal',montantDemande:'4000',dureeRemboursement:'12',typeRachat:'Sans engagement'},
    //   {id:11,datedemande:'08/02/2018',motifDemande:'Equipment',typePret:'Normal',montantDemande:'240040',dureeRemboursement:'20',typeRachat:'Avec engagement'},
    //   {id:12,datedemande:'10/04/2018',motifDemande:'Immobilier',typePret:'Normal',montantDemande:'2400250',dureeRemboursement:'10',typeRachat:'Sans engagement'}
    // ];
    // this.dataSource = data;
    this.getDemandes();

    this.subjService.testSubject.subscribe(
      (data)=>{this.getDemandes()}
    )
  }

  getDemandes(){
    return this.validationsService.getDemandes().subscribe(result=>{
      this.dataSource = result;
    })
  }

  openModalAjout(data){
    const modalRef = this.modalService.open(ShowAssureeComponent, { size: 'lg', backdrop: 'static', keyboard: false });
    modalRef.componentInstance.data = data;
  }
  openModalValidate(data){
    const modalRef = this.modalService.open(ShowConjointComponent, { size: 'lg', backdrop: 'static', keyboard: false });
    modalRef.componentInstance.data = data;
  }


  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift(
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          icon: 'plus',
          onClick: this.openModalAjout.bind(this)
        }
      },{
        location: 'after',
        widget: 'dxButton',
        options: {
          icon: 'refresh',
          onClick: this.getDemandes.bind(this)
        }
      });
  }

  refreshDataGrid() {
    this.dataGrid.instance.refresh();
  }







   /* let d= new Array();

    this.validationsService.getvalidation().subscribe(data =>{
      data.forEach(element => {
        if(element.valideAssuree === false && element.livreAssuree === false){
          d.push(element);
        }
      });
    });

    this.validationsService.getEnfants().subscribe(data =>{
      data.forEach(element => {
        if(element.valideEnfant === false && element.livreEnfant === false){
          d.push(element);
        }
      });
    });

    this.validationsService.getConjoints().subscribe(data =>{
      data.forEach(element => {
        if(element.valideConjoint === false && element.livreConjoint === false){
          d.push(element);
        }
      });
      this.dataSource = d;
      console.log(this.dataSource);
    });
    this.registerChangeInUsers();
  }



  openModalValidationAssuree(data){
    if(data.valideAssuree == false){
      const modalRef = this.modalService.open(ShowAssureeComponent, { size: 'lg', backdrop: 'static', keyboard: false });
      modalRef.componentInstance.data = data;

    }
    if(data.valideConjoint == false){
      const modalRef = this.modalService.open(ShowConjointComponent, { size: 'lg', backdrop: 'static', keyboard: false });
      modalRef.componentInstance.data = data;
    }
    if(data.valideEnfant == false){
      const modalRef = this.modalService.open(ShowEnfantComponent, { size: 'lg', backdrop: 'static', keyboard: false });
      modalRef.componentInstance.data = data;
    }

  }
  openModalValidAssuree(data){
    const modalRef = this.modalService.open(VerifierAssureeComponent, { size: 'lg', backdrop: 'static', keyboard: false });
    modalRef.componentInstance.data = data;

  }
openModalRefusAssuree(data){
  if(data.valideAssuree == false){
    const modalRef = this.modalService.open(RefuserAssureeComponent, { size: 'lg', backdrop: 'static', keyboard: false });
    modalRef.componentInstance.data = data;
  }
  if(data.valideConjoint == false){
    const modalRef = this.modalService.open(RefuserConjointComponent, { size: 'lg', backdrop: 'static', keyboard: false });
    modalRef.componentInstance.data = data;
  }
  if(data.valideEnfant == false){
    const modalRef = this.modalService.open(RefuserEnfantComponent, { size: 'lg', backdrop: 'static', keyboard: false });
    modalRef.componentInstance.data = data;
  }

}
registerChangeInUsers() {
  //this.eventSubscriber = this.eventManager.subscribe('tachesListValidation', (response) => this.refresh());
}

refresh(){
  let d= new Array();

  this.validationsService.getvalidation().subscribe(data =>{
    data.forEach(element => {
      if(element.valideAssuree === false && element.livreAssuree === false){
        d.push(element);
      }
    });
  });

  this.validationsService.getEnfants().subscribe(data =>{
    data.forEach(element => {
      if(element.valideEnfant === false && element.valideEnfant === false){
        d.push(element);
      }
    });
  });

  this.validationsService.getConjoints().subscribe(data =>{
    data.forEach(element => {
      if(element.valideEnfants === false && element.livreEnfants === false){
        d.push(element);
      }
    });
    this.dataSource = d;
    console.log(this.dataSource);
  });
}
}*/

}
