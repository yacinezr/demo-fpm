import {ToastrService} from 'ngx-toastr';
import {VerifierAssureeService} from '../verifier-assuree/verifier-assuree.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ShowEnfantService } from 'app/pages/form-elements/validations/show-enfant/show-enfant.service';
@Component({
    selector: 'app-show-enfant',
    templateUrl: './show-enfant.component.html',
    encapsulation: ViewEncapsulation.None
  })
  export class ShowEnfantComponent implements OnInit {
      id:any
    tel: string;
    nom: string;
    prenom: string;
    naissance: string;
    sexe: string;
    anl: boolean;
    oa: boolean;
    bas: boolean;
    valideEnfant:boolean = false;
    livreEnfant:boolean =  false;
    prodEnfant:boolean =  false;
    idAss: string;
   dataSource:any;
   dataSource2:any;
   data: any;

    constructor(private showEnfantService : ShowEnfantService, private verifierAssureeService: VerifierAssureeService,public activeModal: NgbActiveModal, public toastrService: ToastrService) { }
  
    ngOnInit() {
      this.id = this.data.id;
      this.tel = this.data.tel;
      this.nom=this.data.nom;
      this.prenom=this.data.prenom;
      this.naissance = this.data.naissance;
      this.sexe=this.data.sexe;
      this.anl=this.data.anl
      this.oa = this.data.oa;
      this.bas=this.data.bas;

  
      this.dataSource = this.data.enfant;
      this.dataSource2 = this.data.conjoint;
  
    }
    verifierDemande(assuree){
      let assureeToUpdate = assuree;
      assureeToUpdate.valideEnfant = true;
      
    console.log(assureeToUpdate)
      this.showEnfantService.updateAssuree(assureeToUpdate).subscribe(result=>{},
        error =>{
          setTimeout(() => {
            this.toastrService.success('Demande d\'immatriculation acceptée!');
      
          })
        }
    );
      this.activeModal.dismiss(true);
    }
    clear() {
      this.activeModal.dismiss('cancel');
    }
  }
  