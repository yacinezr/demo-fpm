import {HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ShowEnfantService {

      urlAssuree = environment.SERVER_API_URL+ 'api/updateEnfant';
      constructor(private http: HttpClient) { }
      updateAssuree(data): Observable<any> {
        return this.http.post(this.urlAssuree + '.json', data);

    }
    }
