import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { VerifierAssureeService } from 'app/pages/form-elements/validations/verifier-assuree/verifier-assuree.service';

@Component({
  selector: 'app-verifier-assuree',
  templateUrl: './verifier-assuree.component.html',
  styleUrls: ['./verifier-assuree.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class VerifierAssureeComponent implements OnInit {
  
  constructor(public activeModal: NgbActiveModal, private verifierAssureeService: VerifierAssureeService) { }
  ngOnInit() {
  }

  verifierDemande(assuree){
    let assureeToUpdate = assuree;
    assureeToUpdate.valide = true;
    
  console.log(assureeToUpdate)
    this.verifierAssureeService.updateAssuree(assureeToUpdate).subscribe();
    this.activeModal.dismiss(true);
  }
  clear() {
    this.activeModal.dismiss('cancel');
  }
}
