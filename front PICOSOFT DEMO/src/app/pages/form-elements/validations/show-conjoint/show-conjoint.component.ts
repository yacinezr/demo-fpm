import {ToastrService} from 'ngx-toastr';
import {VerifierAssureeService} from '../verifier-assuree/verifier-assuree.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ShowConjointService} from 'app/pages/form-elements/validations/show-conjoint/show-conjoint.service';
import {ValidationsService} from '../validations.service';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-show-conjoint',
  templateUrl: './show-conjoint.component.html',
  encapsulation: ViewEncapsulation.None
})
export class ShowConjointComponent implements OnInit {
  @ViewChild('ff') signupForm: NgForm;
  data: any;
  assureChoosen: any;
  dataSource: any;
  credit:any;
  demande:any;

  decision:any;
  motifDecision:any;
  validation:boolean;

  /*

 id:any
   nom: string;
   prenom: string;
   naissance: string;
   fonction: string;
   cni: string;
   etat: string;
   address:string;
   tel: string;

   anl: boolean;
   pi: boolean;
   ctp: boolean;
   cam: boolean;
   cdbs: boolean;
   ae: boolean;

   valideConjoint:boolean = false;
   livreConjoint:boolean =  false;
   prodConjoint:boolean =  false;
   idAss: string;
   dataSource:any;
   dataSource2:any;
   data: any;

   */
  constructor(public validationsService: ValidationsService, public activeModal: NgbActiveModal, public toastrService: ToastrService) {
  }



  updateDemande() {
    // this.data.decision = this.decision;
    // this.data.motifDecision = this.motifDecision;
    this.validationsService.updateDemande(this.data).subscribe(result => {

        let to = 'sms@picosoft.biz';
        let subject = '216'+this.data.assure.tel;
        let body = 'Votre demande numéro '+this.data.id+' est '+this.data.decision;
        this.validationsService.sendSms(to,subject,body).subscribe(result=>{
          this.activeModal.dismiss('cancel');
        });

      if (this.data.decision === 'Acceptée') {
        this.addCredit();
      }else{
        this.activeModal.dismiss('cancel');
      }
    });
  }

  addCredit() {

    this.credit.numSouscription = this.data.id;
    this.credit.demande.id = this.data.id;


    this.validationsService.addCredit(this.credit).subscribe(result => {
      this.activeModal.dismiss('cancel');
    })
  }

  ngOnInit() {
      this.credit = {
        'total': '',
        'dateDebutRembourcement': '',
        'moisDus': '',
        'demande':{}
      };
    this.validationsService.getCreditByDemande(this.data.id).subscribe(result=>{this.credit = result});

    this.assureChoosen = this.data.assure;
    if(this.data.decision === null){
      this.validation= false;
    }else {
      this.validation= true;
    }
    this.validationsService.getCreditByid(this.assureChoosen.id).subscribe(result => {
      this.dataSource = result;
    })
    /*
    this.id = this.data.id;
    this.nom=this.data.nom;
    this.prenom=this.data.prenom;
    this.naissance = this.data.naissance;
    this.fonction=this.data.fonction;
    this.cni=this.data.cni;
    this.etat= this.data.etat;
    this.address = this.data.address;
    this.tel = this.data.tel;

    this.anl = this.data.anl;
    this.pi = this.data.pi;
    this.ctp = this.data.ctp;
    this.cam = this.data.cam;
    this.cdbs = this.data.cdbs;
    this.ae = this.data.ae;

    this.dataSource = this.data.enfant;
    this.dataSource2 = this.data.conjoint;
*/
  }

  /*
  verifierDemande(assuree){
    let assureeToUpdate = assuree;
    assureeToUpdate.valideConjoint = true;

    console.log(assureeToUpdate)
    this.showConjointService.updateAssuree(assureeToUpdate).subscribe(result=>{},
      error =>{
        setTimeout(() => {
          this.toastrService.success('Demande d\'immatriculation acceptée!');
        })
      }
  );
    this.activeModal.dismiss(true);
  }*/
  clear() {
    this.activeModal.dismiss('cancel');
  }
}
