import {Subscription} from 'rxjs/Subscription';
import { Component, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { ValidationsService } from 'app/pages/form-elements/validations/validations.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ValiderCarteComponent } from 'app/pages/form-elements/wizard/valider-carte/valider-carte.component';
import { ShowAssureeComponent } from 'app/pages/form-elements/validations/show-assuree/show-assuree.component';
import { ShowAssuComponent } from 'app/pages/form-elements/wizard/show-assu/show-assu.component';

@Component({
  selector: 'app-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WizardComponent {
  dataSource: any = {};
  eventSubscriber: Subscription;
  
  constructor(private validationsService : ValidationsService, private modalService: NgbModal) { }
  ngOnInit() {
    this.validationsService.getvalidation().subscribe(data =>{
      console.dir(data);
      let d= new Array();
      data.forEach(element => {
        if(element.valide === true  && element.prod === false && element.livre === false){
          d.push(element);
        }
      });
      this.dataSource = d;
      console.log(this.dataSource)
    });
    this.registerChangeInUsers();
    
  }

  openModalValidationAssuree(data){
    const modalRef = this.modalService.open(ShowAssuComponent, { size: 'lg', backdrop: 'static', keyboard: false });
    modalRef.componentInstance.data = data;
  
  }

  openModalValidationCarte(data){
    const modalRef = this.modalService.open(ValiderCarteComponent, { size: 'lg', backdrop: 'static', keyboard: false });
    console.log(data);
    modalRef.componentInstance.data = data;

  }

  registerChangeInUsers() {
    //this.eventSubscriber = this.eventManager.subscribe('tachesListProduction', (response) => this.refresh());
  }
  refresh(){
    this.validationsService.getvalidation().subscribe(data =>{
      console.dir(data);
      let d= new Array();
      data.forEach(element => {
        if(element.valide === true  && element.prod === false && element.livre === false){
          d.push(element);
        }
      });
      this.dataSource = d;
      console.log(this.dataSource)
    });
  }  


}