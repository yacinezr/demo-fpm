import {ToastrService} from 'ngx-toastr';
import {ValiderCarteService} from '../valider-carte/valider-carte.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {VerifierAssureeService} from '../../validations/verifier-assuree/verifier-assuree.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-show-assu',
  templateUrl: './show-assu.component.html',
  styleUrls: ['./show-assu.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ShowAssuComponent implements OnInit {
  dataSource:any;
  dataSource2:any;
  data: any;
  idAssuree: string
  tel: string;
  nom: string;
  prenom: string;
  naissance: string;
  sexe: string;
  address: string;
  cni: string;
  etat: string;
  fonction: string;
  anl: boolean=false;
  pi: boolean=false;
  ctp: boolean=false;
  cam: boolean=false;
  cdbs: boolean=false;
  ae: boolean=false; 
   constructor(public toastrService: ToastrService,private validerCarteService:ValiderCarteService, private verifierAssureeService: VerifierAssureeService,public activeModal: NgbActiveModal) { }
 
   ngOnInit() {
     this.idAssuree = this.data.idassuree;
     this.tel = this.data.tel;
     this.nom=this.data.nom;
     this.prenom=this.data.prenom;
     this.naissance = this.data.naissance;
     this.sexe=this.data.sexe;
     this.address=this.data.address
     this.cni = this.data.cni;
     this.etat=this.data.etat;
     this.fonction=this.data.fonction
     this.anl = this.data.anl;
     this.pi=this.data.pi;
     this.ctp=this.data.ctp;
     this.cam = this.data.cam;
     this.cdbs=this.data.cdbs;
     this.ae=this.data.ae;
 
     this.dataSource = this.data.enfant;
     this.dataSource2 = this.data.conjoint;
 
   }
   clear() {
     this.activeModal.dismiss('cancel');
   }

}
