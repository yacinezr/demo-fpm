import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ValiderCarteService } from 'app/pages/form-elements/wizard/valider-carte/valider-carte.service';

@Component({
  selector: 'app-valider-carte',
  templateUrl: './valider-carte.component.html',
  styleUrls: ['./valider-carte.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ValiderCarteComponent implements OnInit {
data: any
  constructor(public activeModal: NgbActiveModal, private validerCarteService: ValiderCarteService) { }
  ngOnInit() {
  }

  verifierCarte(assuree){
    let assureeToUpdate = assuree;
    assureeToUpdate.prod = true;
    
    console.log(assureeToUpdate)
    this.validerCarteService.updateAssuree(assureeToUpdate).subscribe();
    this.activeModal.dismiss(true);
  }
  clear() {
    this.activeModal.dismiss('cancel');
  }
}
