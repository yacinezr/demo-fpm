import {Subscription} from 'rxjs/Subscription';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {OnInit, ViewEncapsulation, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import {NgModule, Component, Inject, enableProdMode} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {Http} from '@angular/http';
import {DxDataGridModule, DxDataGridComponent} from 'devextreme-angular';
import CustomStore from 'devextreme/data/custom_store';
import {Gestion2Component} from 'app/pages/form-elements/gestion/gestion2/gestion2.component';
import {AllService} from '../all.service';
import {SubjService} from '../subj.service';

@Component({
  selector: 'app-gestion',
  templateUrl: './gestion.component.html',
  encapsulation: ViewEncapsulation.None
})
export class GestionComponent implements OnInit {

  dataSource: any;
  @ViewChild(DxDataGridComponent) dataGrid: DxDataGridComponent;

  constructor(private subjService :SubjService,private modalService: NgbModal, public allService: AllService) {
  }

  ngOnInit() {
    this.getAll();

    this.subjService.testSubject.subscribe(
      (data)=>{this.getAll()}
    )
  }

  getAll() {
    this.allService.getEchancier().subscribe((result: any) => {
      result.map(item => {
        item.credit.demande.assure.nomprenom = item.credit.demande.assure.nom + ' ' + item.credit.demande.assure.prenom;
        item.montantRemboursement = Number(item.montantRemboursement.replace(/\s/g, ''));
        item.credit.total = Number(item.credit.total.replace(/\s/g, ''));
        item.mensualite = Number(item.mensualite.replace(/\s/g, ''));

        return item;
      });
      // result.forEach((item, index)=>{
      //   item.credit.demande.assure.nomprenom = item.credit.demande.assure.prenom +' '+ item.credit.demande.assure.nom
      // });
      console.log('+++++++++++++++++++++++++ +++++++++++++++++++++++++++++++');
      console.log(result);
      this.dataSource = result;
    })
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'after',
      widget: 'dxButton',
      options: {
        icon: 'refresh',
        onClick: this.getAll.bind(this)
      }
    });
  }

  refreshDataGrid() {
    this.dataGrid.instance.refresh();
  }

  openModalConsult(data) {
    const modalRef = this.modalService.open(Gestion2Component, {size: 'lg', backdrop: 'static', keyboard: false});
    modalRef.componentInstance.data = data;
  }

}
