import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ValidationsService} from '../validations/validations.service';
import {Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import { ImprimerComponent } from 'app/pages/form-elements/consultation/imprimer/imprimer.component';
import { Imprimer2Component } from 'app/pages/form-elements/consultation/imprimer2/imprimer2.component';
import {ImprimerService} from './imprimer/imprimer.service';
import {DxDataGridComponent} from 'devextreme-angular';
import {SubjService} from '../subj.service';

@Component({
  selector: 'app-consultation',
  templateUrl: './consultation.component.html',
  styleUrls: ['./consultation.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [ImprimerService]
})
export class ConsultationComponent implements OnInit ,OnDestroy {
  @ViewChild(DxDataGridComponent) dataGrid: DxDataGridComponent;
  dataSource: any;
  constructor(private subjService :SubjService,private modalService: NgbModal, public imprimerService: ImprimerService) { }

  getAllAssure(){
    return this.imprimerService.getAllAssure().subscribe(result =>{
      this.dataSource = result;
    });
  }

  ngOnInit() {
    this.getAllAssure();

    this.subjService.testSubject.subscribe(
      (data)=>{this.getAllAssure()}
    )

    // ngOnDestroy() {
    //   this.subjService.testSubject.unsubscribe();
    // }
    //
    // let data = [
    //   {id:1,matricule:'AR0087001',nom:'Beaudoin',prenom:'Ray',naissance:'14/10/1962',armee:'Terre',grade:'Général d\'armée'},
    //   {id:2,matricule:'AR0087002',nom:'Paradis',prenom:'Maurice',naissance:'18/10/1974',armee:'Marine nationale',grade:'Amiral'},
    //   {id:3,matricule:'AR0087003',nom:'Parenteau',prenom:'Benjamin',naissance:'08/10/1983',armee:'Air',grade:'Général d\'armée aérienne'},
    //   {id:4,matricule:'AR0087004',nom:'Sauvé',prenom:'Pierre',naissance:'30/10/1977',armee:'Terre',grade:'Commandant'},
    //   {id:5,matricule:'AR0087005',nom:'Louis',prenom:'Dreux',naissance:'06/10/1966',armee:'Air',grade:'Colonel'},
    //   {id:6,matricule:'AR0087006',nom:'Dumoulin',prenom:'Laurent',naissance:'22/10/1980',armee:'Air',grade:'Lieutenant'},
    //   {id:7,matricule:'AR0087007',nom:'Gareau',prenom:'Denis',naissance:'17/10/1969',armee:'Marine nationale',grade:'Capitaine de vaisseau'},
    //   {id:8,matricule:'AR0087008',nom:'Mazuret',prenom:'Agrican',naissance:'01/10/1972',armee:'Terre',grade:'Colonel major'},
    //   {id:9,matricule:'AR0087009',nom:'Frappier',prenom:'Hamilton',naissance:'28/10/1980',armee:'Marine nationale',grade:'Aspirant'},
    //   {id:10,matricule:'AR0087010',nom:'Boucher',prenom:'Forrest',naissance:'31/10/1967',armee:'Air',grade:'Adjudant chef major'},
    //   {id:11,matricule:'AR0087011',nom:'Dandonneau',prenom:'Andrée',naissance:'02/10/1975',armee:'Terre',grade:'Général de corps d\'armée'}
    // ];
    // this.dataSource = data;
  }

  ngOnDestroy() {
    // this.subjService.testSubject.unsubscribe();
  }


  openModalAjout(){
    const modalRef = this.modalService.open(ImprimerComponent, { size: 'lg', backdrop: 'static', keyboard: false });
  }
  openModalUpdate(data){
    const modalRef = this.modalService.open(Imprimer2Component, { size: 'lg', backdrop: 'static', keyboard: false });
    modalRef.componentInstance.data = data;
  }

  /*
  dataSource:any;
  dataSource2:any;
  data: any;
  idAssuree: string
  tel: string;
  nom: string;
  prenom: string;
  naissance: string;
  sexe: string;
  address: string;
  cni: string;
  etat: string;
  fonction: string;
  anl: boolean=false;
  pi: boolean=false;
  ctp: boolean=false;
  cam: boolean=false;
  cdbs: boolean=false;
  ae: boolean=false;
  constructor(private validationsService : ValidationsService, private modalService: NgbModal) { }
  ngOnInit() {
    this.validationsService.getvalidation().subscribe(data =>{
      console.dir(data);
      let d= new Array();
      data.forEach(element => {
        if(element.valideAssuree === false && element.livreAssuree === false){
          d.push(element);
        }
      });
      this.dataSource = d;
      console.log(this.dataSource)
    });
  }
  openModalValidationAssuree(data){
    const modalRef = this.modalService.open(ImprimerComponent, { size: 'lg', backdrop: 'static', keyboard: false });
    modalRef.componentInstance.data = data;

  }
  */


  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift(
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          icon: 'plus',
          onClick: this.openModalAjout.bind(this)
        }
      },{
        location: 'after',
        widget: 'dxButton',
        options: {
          icon: 'refresh',
          onClick: this.getAllAssure.bind(this)
        }
      });
  }

  refreshDataGrid() {
    this.dataGrid.instance.refresh();
  }


}
