import {environment} from 'environments/environment';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class ImprimerService {
  url = environment.SERVER_API_URL + 'api/assures';

  constructor(private http: HttpClient) {
  }

  addAssuree(data): Observable<any> {
    return this.http.post(this.url, data)
    // .map(this.extractData);
    // .catch(this.handleError);
  }

  getAllAssure() {
    return this.http.get(this.url)
  }

  getAssureById(assureId) {
    return this.http.get(this.url+"/"+assureId)
  }

  updateAssure(data): Observable<any> {
    return this.http.put(this.url, data)
    // .map(this.extractData);
    // .catch(this.handleError);
  }



  // private extractData(res: Response) {
  //     // tslint:disable-next-line:curly
  //     if (res)
  //         return res || {};
  //     // tslint:disable-next-line:curly
  //     else
  //         return null;
  // }

  // private handleError(error: Response | any) {
  //     console.error(error.message || error);
  //     return Observable.throw(error.message || error);
  // }
}
